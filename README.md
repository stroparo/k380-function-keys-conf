# k380-function-keys-conf
Make function keys on Logitech k380 bluetooth keyboard default

This was forked at github from:

<https://github.com/jergusg/k380-function-keys-conf>

Which in turn was based on:

<http://www.trial-n-error.de/posts/2012/12/31/logitech-k810-keyboard-configurator/>

<https://github.com/embuc/k480_conf>

Look there for more information.
